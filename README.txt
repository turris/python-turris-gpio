This package provides a class to control the GPIO on the Turris Router and on Turris Omnia.

It is based on Ben Croston's RPi.GPIO library - the API should be almost the same, except the module
name, which is turris_gpio instead of RPi.GPIO. Also, pin numbers and some of the capabilities are
different due to the differences in devices' SoCs.

Note that this module is unsuitable for real-time or timing critical applications.  This is because you
can not predict when Python will be busy garbage collecting.  It also runs under the Linux kernel which
is not suitable for real time applications - it is multitasking O/S and another process may be given
priority over the CPU, causing jitter in your program.  If you are after true real-time performance and
predictability, buy yourself an Arduino http://www.arduino.cc !

Note that the current release does not support SPI, I2C, hardware PWM or serial functionality on the RPi yet.

Although hardware PWM is not available on Turris Router and impossible on Turris Omnia Router, however software
PWM is available to use on all channels.

For examples and documentation, visit upstream project http://sourceforge.net/p/raspberry-gpio-python/wiki/Home/


Notes for devs
==============

To merge changes from upstream, use a Mercurial upstream with the official repository to merge/cherry-pick
changes from. It should be easily done with the help of git-remote-hg: https://github.com/felipec/git-remote-hg

With git-remote-hg installed, simply do:

    git remote add upstream "hg::http://hg.code.sf.net/p/raspberry-gpio-python/code"
