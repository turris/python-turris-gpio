/*
Copyright (c) 2016 CZ.NIC, z. s. p. o.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>
#include "c_gpio.h"
#include "event_gpio.h"

#define ARMADA_38x_REG_BASE		0xf1000000
#define GPIO0_REG_BASE			0x00018100
#define GPIO32_REG_BASE_ADD		0x00000040

#define CONTROL_SET_REG 0x28
#define CONTROL_CLEAR_REG 0x2c
#define OUT_REG 0x00
#define CONTROL_REG 0x04
#define OUT_SET_REG 0x30
#define OUT_CLEAR_REG 0x34
#define IN_REG 0x10
#define IN_POLARITY_REG 0x0c

#define PAGESIZE		sysconf(_SC_PAGESIZE)

static volatile uint32_t *gpio_map;
static uint32_t gpio_exported[2] = {0, 0};

int is_gpio_exported(int gpio)
{
    if (gpio > 31)
        return (gpio_exported[1]&(1<<(gpio-32)));
    else
        return (gpio_exported[0]&(1<<gpio));
}

void set_gpio_exported(int gpio)
{
    if (gpio > 31)
        gpio_exported[1]|=(1<<(gpio-32));
    else
        gpio_exported[0]|=(1<<gpio);
}

int setup(void)
{
    int mem_fd;

    if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0)
        return SETUP_DEVMEM_FAIL;

    gpio_map = (uint32_t *)mmap(NULL, PAGESIZE, PROT_READ|PROT_WRITE, MAP_SHARED, mem_fd, (ARMADA_38x_REG_BASE+GPIO0_REG_BASE)&(~(PAGESIZE-1)));

    if ((int)gpio_map == -1)
        return SETUP_MMAP_FAIL;


    return SETUP_OK;
}

static inline uint32_t readl(const volatile void *addr)
{
    return *((uint32_t *)addr);
}

static inline void writel(uint32_t val, volatile void *addr)
{
    *((volatile uint32_t *)addr) = val;
}

static uint32_t get_base(int gpio)
{
	return (((uint32_t)gpio_map)+(GPIO0_REG_BASE&(PAGESIZE-1))+((gpio<32)?0:GPIO32_REG_BASE_ADD));
}

static int get_shift(int gpio)
{
	return (gpio<32)?gpio:(gpio-32);
}

void setup_gpio(int gpio, int direction, int pud)
{
    uint32_t reg = get_base(gpio);

    if (!is_gpio_exported(gpio)) {
        gpio_export(gpio);
        set_gpio_exported(gpio);
    }

    if (direction == INPUT) {
        writel((1 << get_shift(gpio)), (void *)(reg+CONTROL_SET_REG));
        writel((readl((void *)(reg+IN_POLARITY_REG))&(~(1<<get_shift(gpio)))),
                (void *)(reg+IN_POLARITY_REG));
    } else
        writel((1 << get_shift(gpio)), (void *)(reg+CONTROL_CLEAR_REG));
}

int gpio_function(int gpio)
{
    uint32_t reg=get_base(gpio)+CONTROL_REG;
    /* 0=input, 1=output, register is the opposite */
    return !(readl((void*)reg)&(1<<get_shift(gpio)));
}

void output_gpio(int gpio, int value)
{
    int reg = get_base(gpio);

    if (value) /* value == HIGH */
        writel((1 << get_shift(gpio)), (void *)(reg+OUT_SET_REG));
    else
        writel((1 << get_shift(gpio)), (void *)(reg+OUT_CLEAR_REG));
}

int input_gpio(int gpio)
{
    int reg=get_base(gpio)+IN_REG;
    /* 0=LOW, 1=HIGH, register is the same unless polarity
       register is not zero. */
    return (readl((void *)reg)&(1<<get_shift(gpio)));
}

void cleanup(void)
{
    munmap((void *)gpio_map, PAGESIZE);
}
