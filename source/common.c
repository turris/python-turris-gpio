/*
Copyright (c) 2015 CZ.NIC, z. s. p. o. <https://www.nic.cz/>
Copyright (c) 2013-2014 Ben Croston

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "Python.h"
#include "c_gpio.h"
#include "common.h"

int gpio_mode = MODE_UNKNOWN;
#ifdef BOARD_OMNIA
const int pin_to_gpio_rev1[10] = {18, 33, 34, 35, 36, 42, 44, 47, 51, 56};	//Turris Omnia
#else
const int pin_to_gpio_rev1[11] = {-1, -1, 15, 0, 6, 1, 5, 2, 4, 3, -1};	//Turris 1.0 pin number to gpio mapping
#endif

int setup_error = 0;
int module_setup = 0;

int check_gpio_priv(void)
{
    // check module has been imported cleanly
    if (setup_error)
    {
        PyErr_SetString(PyExc_RuntimeError, "Module not imported correctly!");
        return 1;
    }

    // check mmap setup has worked
    if (!module_setup)
    {
        PyErr_SetString(PyExc_RuntimeError, "No access to /dev/mem.  Try running as root!");
        return 2;
    }
    return 0;
}

int get_gpio_number(int channel, unsigned int *gpio)
{
    // check setmode() has been run
    if (gpio_mode != BOARD && gpio_mode != SOC)
    {
        PyErr_SetString(PyExc_RuntimeError, "Please set pin numbering mode using GPIO.setmode(GPIO.BOARD) or GPIO.setmode(GPIO.SOC)");
        return 3;
    }

    // check channel number is in range
#ifdef BOARD_OMNIA
    if ( (gpio_mode == SOC && (channel < 0 || channel > 59))
      || (gpio_mode == BOARD && (channel < 0 || channel > 9) && rpiinfo.p1_revision == 1)
#else
    if ( (gpio_mode == SOC && (channel < 0 || channel > 15))
      || (gpio_mode == BOARD && (channel < 1 || channel > 10) && rpiinfo.p1_revision == 1)
#endif
      || (rpiinfo.p1_revision != 1) )
    {
        PyErr_SetString(PyExc_ValueError, "The channel sent is invalid on a Turris router (number out of range)");
        return 4;
    }

    // convert channel to gpio
    if (gpio_mode == BOARD)
    {
        if (*(*pin_to_gpio+channel) == -1)
        {
            PyErr_SetString(PyExc_ValueError, "The channel sent is invalid on a Turris router (pin not gpio)");
            return 5;
        } else {
            *gpio = *(*pin_to_gpio+channel);
        }
    }
    else // gpio_mode == SOC
    {
        *gpio = channel;
    }

    return 0;
}
