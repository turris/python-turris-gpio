Change Log
==========

0.1.2
=====

Merge Support for Turris Omnia with the Turris Router.

0.1.1
=====

Basic Support for Turris Omnia.

0.1.0
=====

Initial version based on RPi.GPIO 0.6.0a3.
