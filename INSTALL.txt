If you are using TurrisOS, all you need to do to install the latest release is:
$ opkg install python-turris-gpio

------------

If you want to build your own version from this downloaded copy, make
sure that you have the Python development source installed first.

To install the module:

$ python setup-turris.py install
  or
$ python setup-omnia.py install
